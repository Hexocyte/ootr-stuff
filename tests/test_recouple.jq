#!/usr/bin/jq -nf
# SPDX-FileCopyrightText: © 2021 Hexocyte
# SPDX-License-Identifier: MIT

reduce inputs as $t({}; .[$t.desc] = ($t.entrances | contains($t.expect_entry)))
