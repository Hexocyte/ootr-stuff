#!/usr/bin/jq -f
# SPDX-FileCopyrightText: © 2021 Hexocyte
# SPDX-License-Identifier: MIT

# recouple.jq
#
# Adds the reverse entrances to an OoTR entrance plando.
#
# Conflicting entrances (i.e. the reverse exit is already set to another
# entrance) result in an error.
#
# Also available as a snippet at JQ Play: https://jqplay.org/s/JVm7Qj6UNQ

def err_fmt:
    "recouple.jq: error, \(.), exiting\n" | halt_error;

def parse_exit:
    # convert "A -> B" form to {"region": "B", "from": "A"}
    . as $exit |
    . / " -> "
    | if length == 2
        then
            { "region": .[1], "from": .[0] }
        elif length == 1 and $exit[:6] == "World "
            then "cannot reverse multiworld entrances (not implemented)"
            | err_fmt
        else
            "malformed exit \"\($exit)\""
            | err_fmt
        end;

def unpack_entrances:
    map(
        .forward |= parse_exit
        | .backward =
            if .backward
                then .backward | parse_exit
                else .forward | { "region": .from, "from": .region }
                end
    );

# entrance list based on the list from EntranceShuffle.py from the randomizer
([
{forward:"KF Outside Deku Tree -> Deku Tree Lobby",type:"dungeon"},
{forward:"Death Mountain -> Dodongos Cavern Beginning",type:"dungeon"},
{forward:"Zoras Fountain -> Jabu Jabus Belly Beginning",type:"dungeon"},
{forward:"SFM Forest Temple Entrance Ledge -> Forest Temple Lobby",type:"dungeon"},
{forward:"DMC Fire Temple Entrance -> Fire Temple Lower",type:"dungeon"},
{forward:"Lake Hylia -> Water Temple Lobby",type:"dungeon"},
{forward:"Desert Colossus -> Spirit Temple Lobby",backward:"Spirit Temple Lobby -> Desert Colossus From Spirit Lobby",type:"dungeon"},
{forward:"Graveyard Warp Pad Region -> Shadow Temple Entryway",type:"dungeon"},
{forward:"Kakariko Village -> Bottom of the Well",type:"dungeon"},
{forward:"ZF Ice Ledge -> Ice Cavern Beginning",type:"dungeon"},
{forward:"Gerudo Fortress -> Gerudo Training Ground Lobby",type:"dungeon"},
{forward:"Gerudo Fortress -> Gerudo Training Grounds Lobby",type:"dungeon"},
{forward:"Kokiri Forest -> KF Midos House"},
{forward:"Kokiri Forest -> KF Sarias House"},
{forward:"Kokiri Forest -> KF House of Twins"},
{forward:"Kokiri Forest -> KF Know It All House"},
{forward:"Kokiri Forest -> KF Kokiri Shop"},
{forward:"Lake Hylia -> LH Lab"},
{forward:"LH Fishing Island -> LH Fishing Hole"},
{forward:"GV Fortress Side -> GV Carpenter Tent"},
{forward:"Market Entrance -> Market Guard House"},
{forward:"Market -> Market Mask Shop"},
{forward:"Market -> Market Bombchu Bowling"},
{forward:"Market -> Market Potion Shop"},
{forward:"Market -> Market Treasure Chest Game"},
{forward:"Market Back Alley -> Market Bombchu Shop"},
{forward:"Market Back Alley -> Market Man in Green House"},
{forward:"Kakariko Village -> Kak Carpenter Boss House"},
{forward:"Kakariko Village -> Kak House of Skulltula"},
{forward:"Kakariko Village -> Kak Impas House"},
{forward:"Kak Impas Ledge -> Kak Impas House Back"},
{forward:"Kak Backyard -> Kak Odd Medicine Building"},
{forward:"Graveyard -> Graveyard Dampes House"},
{forward:"Goron City -> GC Shop"},
{forward:"Zoras Domain -> ZD Shop"},
{forward:"Lon Lon Ranch -> LLR Talons House"},
{forward:"Lon Lon Ranch -> LLR Stables"},
{forward:"Lon Lon Ranch -> LLR Tower"},
{forward:"Market -> Market Bazaar"},
{forward:"Market -> Market Shooting Gallery"},
{forward:"Kakariko Village -> Kak Bazaar"},
{forward:"Kakariko Village -> Kak Shooting Gallery"},
{forward:"Desert Colossus -> Colossus Great Fairy Fountain"},
{forward:"Hyrule Castle Grounds -> HC Great Fairy Fountain",backward:"HC Great Fairy Fountain -> Castle Grounds"},
{forward:"Ganons Castle Grounds -> OGC Great Fairy Fountain",backward:"OGC Great Fairy Fountain -> Castle Grounds"},
{forward:"DMC Lower Nearby -> DMC Great Fairy Fountain",backward:"DMC Great Fairy Fountain -> DMC Lower Local"},
{forward:"Death Mountain Summit -> DMT Great Fairy Fountain"},
{forward:"Zoras Fountain -> ZF Great Fairy Fountain"},
{forward:"Kokiri Forest -> KF Links House"},
{forward:"ToT Entrance -> Temple of Time"},
{forward:"Kakariko Village -> Kak Windmill"},
{forward:"Kakariko Village -> Kak Potion Shop Front"},
{forward:"Kak Backyard -> Kak Potion Shop Back"},
{forward:"Desert Colossus -> Colossus Grotto"},
{forward:"Lake Hylia -> LH Grotto"},
{forward:"Zora River -> ZR Storms Grotto"},
{forward:"Zora River -> ZR Fairy Grotto"},
{forward:"Zora River -> ZR Open Grotto"},
{forward:"DMC Lower Nearby -> DMC Hammer Grotto",backward:"DMC Hammer Grotto -> DMC Lower Local"},
{forward:"DMC Upper Nearby -> DMC Upper Grotto",backward:"DMC Upper Grotto -> DMC Upper Local"},
{forward:"GC Grotto Platform -> GC Grotto"},
{forward:"Death Mountain -> DMT Storms Grotto"},
{forward:"Death Mountain Summit -> DMT Cow Grotto"},
{forward:"Kak Backyard -> Kak Open Grotto"},
{forward:"Kakariko Village -> Kak Redead Grotto"},
{forward:"Hyrule Castle Grounds -> HC Storms Grotto",backward:"HC Storms Grotto -> Castle Grounds"},
{forward:"Hyrule Field -> HF Tektite Grotto"},
{forward:"Hyrule Field -> HF Near Kak Grotto"},
{forward:"Hyrule Field -> HF Fairy Grotto"},
{forward:"Hyrule Field -> HF Near Market Grotto"},
{forward:"Hyrule Field -> HF Cow Grotto"},
{forward:"Hyrule Field -> HF Inside Fence Grotto"},
{forward:"Hyrule Field -> HF Open Grotto"},
{forward:"Hyrule Field -> HF Southeast Grotto"},
{forward:"Lon Lon Ranch -> LLR Grotto"},
{forward:"SFM Entryway -> SFM Wolfos Grotto"},
{forward:"Sacred Forest Meadow -> SFM Storms Grotto"},
{forward:"Sacred Forest Meadow -> SFM Fairy Grotto"},
{forward:"LW Beyond Mido -> LW Scrubs Grotto"},
{forward:"Lost Woods -> LW Near Shortcuts Grotto"},
{forward:"Kokiri Forest -> KF Storms Grotto"},
{forward:"Zoras Domain -> ZD Storms Grotto"},
{forward:"Gerudo Fortress -> GF Storms Grotto"},
{forward:"GV Fortress Side -> GV Storms Grotto"},
{forward:"GV Grotto Ledge -> GV Octorok Grotto"},
{forward:"LW Beyond Mido -> Deku Theater"},
{forward:"Graveyard -> Graveyard Shield Grave"},
{forward:"Graveyard -> Graveyard Heart Piece Grave"},
{forward:"Graveyard -> Graveyard Royal Familys Tomb"},
{forward:"Graveyard -> Graveyard Composers Grave"},
{forward:"Graveyard -> Graveyard Dampes Grave"},
{forward:"Kokiri Forest -> LW Bridge From Forest",backward:"LW Bridge -> Kokiri Forest",type:"overworld"},
{forward:"Kokiri Forest -> Lost Woods",backward:"LW Forest Exit -> Kokiri Forest",type:"overworld"},
{forward:"Lost Woods -> GC Woods Warp",type:"overworld"},
{forward:"Lost Woods -> Zora River",type:"overworld"},
{forward:"LW Beyond Mido -> SFM Entryway",type:"overworld"},
{forward:"LW Bridge -> Hyrule Field",type:"overworld"},
{forward:"Hyrule Field -> Lake Hylia",type:"overworld"},
{forward:"Hyrule Field -> Gerudo Valley",type:"overworld"},
{forward:"Hyrule Field -> Market Entrance",type:"overworld"},
{forward:"Hyrule Field -> Kakariko Village",type:"overworld"},
{forward:"Hyrule Field -> ZR Front",type:"overworld"},
{forward:"Hyrule Field -> Lon Lon Ranch",type:"overworld"},
{forward:"Lake Hylia -> Zoras Domain",type:"overworld"},
{forward:"GV Fortress Side -> Gerudo Fortress",type:"overworld"},
{forward:"GF Outside Gate -> Wasteland Near Fortress",type:"overworld"},
{forward:"Wasteland Near Colossus -> Desert Colossus",type:"overworld"},
{forward:"Market Entrance -> Market",type:"overworld"},
{forward:"Market -> Castle Grounds",type:"overworld"},
{forward:"Market -> ToT Entrance",type:"overworld"},
{forward:"Kakariko Village -> Graveyard",type:"overworld"},
{forward:"Kak Behind Gate -> Death Mountain",type:"overworld"},
{forward:"Death Mountain -> Goron City",type:"overworld"},
{forward:"GC Darunias Chamber -> DMC Lower Local",backward:"DMC Lower Nearby -> GC Darunias Chamber",type:"overworld"},
{forward:"Death Mountain Summit -> DMC Upper Local",backward:"DMC Upper Nearby -> Death Mountain Summit",type:"overworld"},
{forward:"ZR Behind Waterfall -> Zoras Domain",type:"overworld"},
{forward:"ZD Behind King Zora -> Zoras Fountain",type:"overworld"}
] | unpack_entrances) as $entrance_pairs |

def get_pair(f):
    first(($entrance_pairs[] | select(f)), null);

def reverse_connection:
    . as $c |
    get_pair(.forward == $c).backward // get_pair(.backward == $c).forward;

def fmt_entrance:
    # represent the entrance as an object or string for the plando

    # .type is null for interior and graves/grottos,
    # a string for dungeon and overworld
    . as $entrance |
    if get_pair((.forward == $entrance and .type) or .backward == $entrance)
        then .
        else .region
        end;

def get_entrance:
    # the entrance you come from where if you turn around you go
    # to the input exit, null if the exit is oneway or otherwise unknown
    parse_exit | reverse_connection;

def eval_entrance:
    # interpret entrance from the plando into {"region": "A", "from": "B"} form
    if type == "object" then . else { "region": . } end
    | .from = ( .from // (
        . as $entrance |
        get_pair(.forward.region == $entrance.region and .type != "overworld")
        .forward.from
    ));

def fmt_exit:
    # convert {"region": "A", "from": "B"} form to "B -> A"
    "\(.from) -> \(.region)";

def get_exit:
    # the exit you go to if you turn around from the input entrance
    eval_entrance | reverse_connection
    // ( "unknown entrance '\(.region)' from '\(.from)'" | err_fmt )
    | fmt_exit;

.entrances |= (
    . // ( "missing \"entrances\" section" | err_fmt ) |
    reduce to_entries[] as $e (.;
        ($e.key | get_entrance) as $new_entrance |
        if $new_entrance
            then
                ($e.value | get_exit) as $new_exit |
                if .[$new_exit] | eval_entrance == $new_entrance
                    then
                        # already coupled, leave it as is
                        .
                    elif .[$new_exit]
                        then "mismatched entrances '\($e.key)', '\($new_exit)'"
                        | err_fmt
                    else
                        .[$new_exit] = ($new_entrance | fmt_entrance)
                    end
            else
                # unknown exit from input plando (e.g. oneway), ignore
                .
            end
    )
)
