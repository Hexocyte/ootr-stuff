#!/usr/bin/env python3
# SPDX-FileCopyrightText: © 2021 Hexocyte
# SPDX-License-Identifier: MIT

# custom_pools.py
#
# generate an OoTR plando with custom entrance pools

import argparse
import random
import json
import sys

# entrance list based on the list from EntranceShuffle.py from the randomizer
exit_groups = {
    'dungeon': (
        'KF Outside Deku Tree -> Deku Tree Lobby',
        'Death Mountain -> Dodongos Cavern Beginning',
        'Zoras Fountain -> Jabu Jabus Belly Beginning',
        'SFM Forest Temple Entrance Ledge -> Forest Temple Lobby',
        'DMC Fire Temple Entrance -> Fire Temple Lower',
        'Lake Hylia -> Water Temple Lobby',
        'Desert Colossus -> Spirit Temple Lobby',
        'Graveyard Warp Pad Region -> Shadow Temple Entryway',
        'Kakariko Village -> Bottom of the Well',
        'ZF Ice Ledge -> Ice Cavern Beginning',
        'Gerudo Fortress -> Gerudo Training Ground Lobby'),
    'simple_interior': (
        'Kokiri Forest -> KF Sarias House',
        'Kokiri Forest -> KF House of Twins',
        'Kokiri Forest -> KF Know It All House',
        'Kokiri Forest -> KF Kokiri Shop',
        'Lake Hylia -> LH Lab',
        'LH Fishing Island -> LH Fishing Hole',
        'GV Fortress Side -> GV Carpenter Tent',
        'Market Entrance -> Market Guard House',
        'Market -> Market Mask Shop',
        'Market -> Market Bombchu Bowling',
        'Market -> Market Potion Shop',
        'Market -> Market Treasure Chest Game',
        'Market Back Alley -> Market Bombchu Shop',
        'Market Back Alley -> Market Man in Green House',
        'Kakariko Village -> Kak Carpenter Boss House',
        'Kakariko Village -> Kak House of Skulltula',
        'Kakariko Village -> Kak Impas House',
        'Kak Impas Ledge -> Kak Impas House Back',
        'Kak Backyard -> Kak Odd Medicine Building',
        'Graveyard -> Graveyard Dampes House',
        'Goron City -> GC Shop',
        'Zoras Domain -> ZD Shop',
        'Lon Lon Ranch -> LLR Talons House',
        'Lon Lon Ranch -> LLR Stables',
        'Lon Lon Ranch -> LLR Tower',
        'Market -> Market Bazaar',
        'Market -> Market Shooting Gallery',
        'Kakariko Village -> Kak Bazaar',
        'Kakariko Village -> Kak Shooting Gallery',
        'Desert Colossus -> Colossus Great Fairy Fountain',
        'Hyrule Castle Grounds -> HC Great Fairy Fountain',
        'Ganons Castle Grounds -> OGC Great Fairy Fountain',
        'DMC Lower Nearby -> DMC Great Fairy Fountain',
        'Death Mountain Summit -> DMT Great Fairy Fountain',
        'Zoras Fountain -> ZF Great Fairy Fountain'),
    'special_interior': (
        'Kokiri Forest -> KF Links House',
        'ToT Entrance -> Temple of Time',
        'Kakariko Village -> Kak Windmill',
        'Kakariko Village -> Kak Potion Shop Front',
        'Kak Backyard -> Kak Potion Shop Back'),
    'grotto': (
        'Desert Colossus -> Colossus Grotto',
        'Lake Hylia -> LH Grotto',
        'Zora River -> ZR Storms Grotto',
        'Zora River -> ZR Fairy Grotto',
        'Zora River -> ZR Open Grotto',
        'DMC Lower Nearby -> DMC Hammer Grotto',
        'DMC Upper Nearby -> DMC Upper Grotto',
        'GC Grotto Platform -> GC Grotto',
        'Death Mountain -> DMT Storms Grotto',
        'Death Mountain Summit -> DMT Cow Grotto',
        'Kak Backyard -> Kak Open Grotto',
        'Kakariko Village -> Kak Redead Grotto',
        'Hyrule Castle Grounds -> HC Storms Grotto',
        'Hyrule Field -> HF Tektite Grotto',
        'Hyrule Field -> HF Near Kak Grotto',
        'Hyrule Field -> HF Fairy Grotto',
        'Hyrule Field -> HF Near Market Grotto',
        'Hyrule Field -> HF Cow Grotto',
        'Hyrule Field -> HF Inside Fence Grotto',
        'Hyrule Field -> HF Open Grotto',
        'Hyrule Field -> HF Southeast Grotto',
        'Lon Lon Ranch -> LLR Grotto',
        'SFM Entryway -> SFM Wolfos Grotto',
        'Sacred Forest Meadow -> SFM Storms Grotto',
        'Sacred Forest Meadow -> SFM Fairy Grotto',
        'LW Beyond Mido -> LW Scrubs Grotto',
        'Lost Woods -> LW Near Shortcuts Grotto',
        'Kokiri Forest -> KF Storms Grotto',
        'Zoras Domain -> ZD Storms Grotto',
        'Gerudo Fortress -> GF Storms Grotto',
        'GV Fortress Side -> GV Storms Grotto',
        'GV Grotto Ledge -> GV Octorok Grotto',
        'LW Beyond Mido -> Deku Theater'),
    'grave': (
        'Graveyard -> Graveyard Shield Grave',
        'Graveyard -> Graveyard Heart Piece Grave',
        'Graveyard -> Graveyard Royal Familys Tomb',
        'Graveyard -> Graveyard Dampes Grave'),
    'owl_drop': (
        'LH Owl Flight -> Hyrule Field',
        'DMT Owl Flight -> Kak Impas Rooftop'),
    'spawn': (
        'Child Spawn -> KF Links House',
        'Adult Spawn -> Temple of Time'),
    'warp_song': (
        'Minuet of Forest Warp -> Sacred Forest Meadow',
        'Bolero of Fire Warp -> DMC Central Local',
        'Serenade of Water Warp -> Lake Hylia',
        'Requiem of Spirit Warp -> Desert Colossus',
        'Nocturne of Shadow Warp -> Graveyard Warp Pad Region',
        'Prelude of Light Warp -> Temple of Time'),
    'extra': (
        'ZD Eyeball Frog Timeout -> Zoras Domain',
        'ZR Top of Waterfall -> Zora River'),
    '': ()
}

tot_warp = {'region': 'Temple of Time',
            'from': 'Prelude of Light Warp'}

kf_bed = {'region': 'KF Links House',
          'from': 'Child Spawn'}

grave_warp = {'region': 'Graveyard Warp Pad Region', 
              'from': 'Nocturne of Shadow Warp'}

twoways = {'dungeon', 'simple_interior', 'special_interior', 'grotto', 'grave'}
oneways = {'owl_drop', 'spawn', 'warp_song', 'extra'}

def get_args():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter,description=
        'generate an OoTR plando with custom entrance pools\n'
        ' \n'
        "Unlike the randomizer's entrance shuffling, this program cannot ensure all\n"
        'locations are reachable, so depending on what pools you specify and what\n'
        'settings you use in the randomizer, you can have a very high chance of making a\n'
        "plando that won't work with your settings. Therefore it's best to shuffle only a\n"
        'few entrances here and leave the rest to the randomizer.\n'
        ' \n'
        'Outputs the plando to standard output. When using the plando, remember to set\n'
        'all necessary settings, including shuffling all entrance types that are shuffled\n'
        'in the plando. Mixing entrance types in a two-way pool (except \n'
        'simple_interior+special_interior and grotto+grave) requires the Mix Entrance\n'
        'Pools setting, which is only available in the Dev-R fork.\n'
        ' \n'
        'This program cannot shuffle normal overworld entrances, and with decoupled, it\n'
        'only sets forward entrances (from the overworld to the building/dungeon/etc).\n'
    )
    parser.add_argument('pool', nargs='+', action='extend', help=
        'a group of entrance types to be shuffled together, separated by \n'
        'pluses (+).\n'
        'entrance types are:\n'
        '* dungeon\n'
        '* simple_interior\n'
        '* special_interior - note that this does not ensure both Kak\n'
        '                     Potion Shop entrances are in the same hint\n'
        '                     area, so the resulting plando might be\n'
        '                     unusable unless all hints are disabled\n'
        '* grotto           - grottos, excluding graves in Graveyard\n'
        '* grave            - graves in Graveyard\n'
        '* owl_drop\n'
        '* spawn\n'
        '* warp_song\n'
        "* extra            - 2 extra entrances—1 in King Zora's Throne\n"
        '                     Room normally accessed by the frog timer\n'
        "                     expiring and 1 at the top waterfall in Zora's\n"
        '                     River, normally unused. note that shuffling\n'
        '                     this does not affect the warp when the frog\n'
        '                     timer expires if the timer exists in your\n'
        '                     settings, and two entrances in the pool will\n'
        '                     be unused.\n'
        'entrance types not used in any pool will be excluded from the\n'
        'plando, so the randomizer will handle them'
    )
    return parser.parse_args()

def shuffle_entrances(pools):
    entrances_section = {}
    for expr in pools:
        exits, entrances = eval_pool(expr)
        if len(entrances) > len(exits) and entrances.count(tot_warp) == 2:
            entrances.remove(tot_warp)
        random.shuffle(entrances)
        # set owl drops only in the overworld
        for exit in exit_groups['owl_drop']:
            if exit in exits:
                for entrance in entrances:
                    if entrance not in (tot_warp, kf_bed):
                        entrances_section[exit] = entrance
                        entrances.remove(entrance)
                        break
                exits.remove(exit)
        # if the Graveyard warp pad has no exit match,
        # put it later in the list so it has a match
        if grave_warp in entrances[:-len(exits)]:
            new_index = len(entrances) + 1 - random.randrange(len(exits))
            entrances.insert(new_index, grave_warp)
        for exit in exits:
            if exit in entrances_section:
                raise Exception('an entrance is in multiple pools')
            entrances_section[exit] = entrances.pop()
    return entrances_section

def eval_pool(expr):
    exits, entrances = [], []
    groups = expr.split('+')
    if not oneways.isdisjoint(groups) and not twoways.isdisjoint(groups):
        raise Exception('cannot mix one-way and two-way pools')
    for group_name in groups:
        group_name = group_name.strip().lower()
        group = exit_groups[group_name]
        if group_name != 'extra':
            exits.extend(group)
        if group_name in oneways or group_name=='dungeon':
            # oneway, dungeon, and overworld entrances
            # are represented as dictionaries in plando
            for exit in group:
                from_region, region = exit.split(' -> ')
                if from_region == 'Adult Spawn':
                    from_region = 'Prelude of Light Warp'
                entrances.append({'region': region, 'from': from_region})
        elif group_name in twoways:
            # interior, grotto, and grave entrances are represented as strings
            entrances.extend(exit[exit.find(' -> ')+4:] for exit in group)
    return exits, entrances

entrances_section = shuffle_entrances(get_args().pool)
json.dump({'entrances': entrances_section}, sys.stdout, indent=2)
print() # newline
