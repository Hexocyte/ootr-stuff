* SPDX-FileCopyrightText: © 2021 Hexocyte
* SPDX-License-Identifier: MIT

# ootr-stuff

## Scripts

The `scripts/` directory has two command-line scripts for making entrance
plandos for [OoT Randomizer](https://ootrandomizer.com/). They're mainly for
mixed entrance pools and decoupled entrances, which are only available on the
[Dev-R fork](https://github.com/Roman971/OoT-Randomizer), but they could also be
useful for the stable randomizer.

### `recouple.jq`

Add the reverse entrances to an input plando.

Requires [JQ](https://stedolan.github.io/jq/). Alternatively if you don't want
to install anything or if you're terminal-averse, it's available as a snippet at
JQ Play, so you can use it from your browser: https://jqplay.org/s/JVm7Qj6UNQ

Reads an input plando from standard input or by filename as a command-line
argument and writes to standard output.

Conflicting entrances (i.e. the reverse exit is already set to another entrance)
result in an error. The script is compatible with versions 6.0 and 6.1 of the
randomizer. In the input plando, use the region names from the version you are
using for renamed regions (*Gerudo Training Ground Lobby* and *Graveyard Royal
Familys Tomb* in 6.1).

### `custom_pools.py`

Shuffle entrances in custom pools.

Unlike the randomizer's entrance shuffling, this script cannot ensure that all
locations are reachable or that interior entrances follow the same-hint-area
requirement, so depending on what pools you specify and what settings you use in
the randomizer, you can have a very high chance of making a plando that won't
work with your settings. Therefore it's best to shuffle only a few entrances
with it and leave the rest to the randomizer.

Pools are set by command-line arguments expressed as groups of entrance types
separated by pluses (`+`). Entrance types are:

* `dungeon`
* `simple_interior`
* `special_interior`
* `grotto` — Grottos, excluding graves in Graveyard
* `grave` — Graves in Graveyard
* `owl_drop`
* `spawn`
* `warp_song`
* `extra` — 2 extra entrances—1 in King Zora's Throne Room normally accessed by the frog timer expiring and 1 at the top waterfall in Zora's River, normally unused. Note that shuffling this does not affect the warp when the frog timer expires if the timer exists in your settings, and two entrances in the pool will be unused.

Outputs the plando to standard output. When using the plando, remember to set
all necessary settings, including shuffling all entrance types that are shuffled
in the plando. Mixing entrance types in a two-way pool (except
`simple_interior+special_interior` and `grotto+grave`) requires the Mix Entrance
Pools setting, which is only available in the Dev-R fork.

This program cannot shuffle normal overworld entrances, and with decoupled, it
only sets forward entrances (from the overworld to the building/dungeon/etc). It
is compatible with OoT Randomizer v6.1. It may not work with older versions due
to renamed regions.

### Example pipeline

    custom_pools.py grotto+grave | recouple.jq > plando1.json

Shuffles the grottos/graves, adds the reverse entrances, and writes to 
`plando1.json` so that you can play with mixed-pool decoupled entrances except
with unmixed coupled grottos.
